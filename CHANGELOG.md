# Changelog

## [1.2.0] - 2023/7/4

### 新增

- 托管逻辑 - 排序
- 钩子 - BEFORE_SORT，BEFORE_SUBMIT_SORT， AFTER_SUBMIT_SORT
### 变更

- changeSort -> changeOrder

## [1.1.0] - 2022/12/10

### 新增

- 托管逻辑 - 导入
- 钩子 - BEFORE_EDIT_QUERY，BEFORE_VIEW_QUERY，BEFORE_IMPORT，AFTER_IMPORT
- 所有包含异步请求的钩子都可通过 cancel 参数取消，详见文档
- element-ui API - useCrud，useCruds

### 优化

- element-ui 的 lookUpCrud 查找逻辑
- README 内容

## [1.0.2] - 2022/10/23

### 变更

- func.js 升级到 2.0，获取更好的类型支持

## [1.0.0] - 2022/9/17 ⚠️BreakChange

### 重构

- 基于 TypeScript 重构，不兼容旧版本!!!

### 新增

- element-plus 支持
- 事件托管函数，可自定义是否托管如排序变更，选中项变更等

### 移除

- element-plus 版本不需要在视图中标注 crud
- formVM 属性
- doValidate 接口
- table/pagination/form 事件的自动托管
- 视图组件不再需要 crud 标识

### 变更

- submit(formEl)方法接受一个 el-form 组件实例为参数
- table.rowKey 支持全局默认值

## [0.9.0] - 2022/5/30

### 新增

- 接口 doValidate
- APIs getDetails

### 优化

- toDelete 后不自动进行刷新操作，可以在 HOOK 中实现当前页刷新或跳转等

## [0.8.5] - 2022/5/23

### 优化

- toQuery & toExport 接口发送的 GET 请求不再将参数拼接到 url 上，而是直接返回给 request 自行处理以便根据需求对参数进行格式化，比如对象数组的传递

## [0.8.3] - 2022/5/9

### 修复

- test demo
- 默认分页属性 total

### 变更

- README.md

## [0.8.1] - 2022/4/19

### 修复

- SFC 中 CRUD.mixins 查询 crud 异常

## [0.8.0] - 2022/3/21

### 新增

- view.error -> 错误信息{name，message，status}。可以用来监控并作出合适的反馈，比如网络超时提示

### 变更

- 优化打包容量

## [0.7.0] - 2022/2/10

### 新增

- VM -> params 可以向 CRUD 实例传递参数

### 变更

- CRUD 构造参数支持对象形式，并通过 url 属性接收 restURL
- element-ui -> 通过 cruds 标识支持多实例场景
- README.md

## [0.6.0] - 2022/2/3

### 新增

- 在执行 toEdit/toView/toDelete 时，会检查 rowKey 属性并给出警告
- element-ui -> 增加 el-tree 托管，可以像 el-table 样使用 el-tree

### 变更

- 构建文件
- README.md

## [0.5.0] - 2022/1/29

### 新增

- API setURLParams

### 变更

- getRestURL 支持 URL 参数
- README.md

## [0.4.0] - 2022/1/26

### 新增

- API toView
- CRUD.HOOK.BEFORE_VIEW

### 移除

- element-ui -> el-table 删除行编辑模式

## [0.3.2] - 2022/1/1

### 新增

- element-ui -> crud.formVM 可以访问 vue el-form 对象
- view.table.rowKey -> 表格行 id key

## [0.3.1] - 2021/12/29

### 变更

- submit 完成后不会变更表单状态

## [0.3.0] - 2021/12/28

### 新增

- view.loading.form
- CRUD.HOOK.ON_CANCEL
- element-ui -> table.vm 可以访问 vue el-table 对象
- element-ui -> el-table 新增行编辑模式，通过 row-edit 标记开启

### 变更

- 更新 readme

## [0.2.1] - 2021/12/20

### 修复

- view 默认值 bug

### 变更

- 升级 func.js 版本

## [0.2.0] - 2021/12/16

### 新增

- 支持分页/view 的全局默认值设置
- README 增加全局默认值说明

## [0.1.5] - 2021/12/11

### 变更

- 修改文件名
- 升级 func.js 版本
- 修改 README

## [0.1.4] - 2021/12/10

### 变更

- table.selections -> table.selection

## [0.1.3] - 2021/12/10

### 变更

- 修改 README
- 增加 welcome 信息

## [0.1.2] - 2021/11/22

### 变更

- 修改 README

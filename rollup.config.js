/* eslint-disable max-len */
import commonjs from '@rollup/plugin-commonjs'
import { terser } from 'rollup-plugin-terser'
import banner2 from 'rollup-plugin-banner2'
import json from '@rollup/plugin-json'
import copy from 'rollup-plugin-copy'
import typescript from 'rollup-plugin-typescript2'
import clear from 'rollup-plugin-clear'

const fs = require('fs')
var path = require('path')

function readfilelist(dir, fileslist = []) {
  const files = fs.readdirSync(dir)
  files.forEach((item, index) => {
    var fullpath = path.join(dir, item)
    const stat = fs.statSync(fullpath)
    if (stat.isDirectory()) {
      readfilelist(path.join(dir, item), fileslist)
    } else {
      fileslist.push([fullpath, item])
    }
  })
  return fileslist
}

process.on('exit', () => {
  //index
  readfilelist('./dist/types').forEach((path) => {
    fs.copyFileSync(path[0], './dist/' + path[1])
  })
  fs.rmSync('./dist/types', { recursive: true })
  //plus
  readfilelist('./dist/element-plus/types').forEach((path) => {
    fs.copyFileSync(path[0], './dist/element-plus/' + path[1])
  })
  fs.rmSync('./dist/element-plus/types', { recursive: true })
  //ui
  readfilelist('./dist/element-ui').forEach((path) => {
    fs.copyFileSync(path[0], './dist/element-ui/' + path[1])
  })
  fs.rmSync('./dist/element-ui/types', { recursive: true })
})

const pkg = require('./package.json')

const targets = [
  {
    input: './src/index.ts',
    plugins: [
      clear({
        targets: ['dist'],
      }),
      typescript({
        clean: true,
        useTsconfigDeclarationDir: true,
        tsconfigOverride: {
          compilerOptions: {
            declarationDir: './dist/types',
          },
        },
        include: ['./src/index.ts', './src/types.ts'],
      }),
      commonjs(),
      banner2(
        () => `/**
   * ${pkg.name} v${pkg.version}
   * ${pkg.description}
   * @${pkg.author}
   * ${pkg.repository.url}
   */
  `
      ),
      json(),
      copy({
        targets: [
          {
            src: [
              'CHANGELOG.md',
              'LICENSE',
              'README.md',
              'package.json',
              '.npmignore',
            ],
            dest: 'dist',
          },
        ],
      }),
    ],
    output: [
      {
        file: 'dist/index.js',
        format: 'esm',
        plugins: [terser()],
      },
    ],
  },
  {
    input: 'src/element-plus/index.ts',
    external: ['vue', 'element-plus', '../index'],
    plugins: [
      typescript({
        clean: true,
        useTsconfigDeclarationDir: true,
        tsconfigOverride: {
          compilerOptions: {
            declarationDir: './dist/element-plus/types',
          },
        },
        include: ['src/element-plus/index.ts'],
      }),
      commonjs(),
      banner2(
        () => `/**
   * ${pkg.name}/crud-element-plus v${pkg.version}
   * ${pkg.description}
   * @${pkg.author}
   * ${pkg.repository.url}
   */
  `
      ),
      json(),
      copy({
        targets: [
          {
            src: ['package.json'],
            dest: 'dist/element-plus',
          },
        ],
      }),
    ],
    output: [
      {
        file: './dist/element-plus/index.js',
        format: 'esm',
        plugins: [terser()],
      },
    ],
  },
  {
    input: 'src/element-ui/index.ts',
    external: ['../index'],
    plugins: [
      typescript({
        clean: true,
        useTsconfigDeclarationDir: true,
        tsconfigOverride: {
          compilerOptions: {
            declarationDir: './dist/element-ui/types',
          },
        },
        include: ['src/element-ui/index.ts'],
      }),
      commonjs(),
      banner2(
        () => `/**
   * ${pkg.name}/element-ui v${pkg.version}
   * ${pkg.description}
   * @${pkg.author}
   * ${pkg.repository.url}
   */
  `
      ),
      json(),
      copy({
        targets: [
          {
            src: ['package.json'],
            dest: 'dist/element-ui',
          },
        ],
      }),
    ],
    output: [
      {
        file: './dist/element-ui/index.js',
        format: 'esm',
        plugins: [terser()],
      },
    ],
  },
]

export default targets
